provider "aws" {
  region  = "eu-west-2"
  version = "~> 2.0"
}

module "lambda_serverless_v0_1_8" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-8"
  describe_function = "Hello Serverless Terraform v0.1.8"

  subdomain = "v0-1-8"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.8/hello.zip"
  s3_bucket = "wh-terraform-hello"
}

module "lambda_serverless_v0_1_9" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-9"
  describe_function = "Hello Serverless Terraform v0.1.9"

  subdomain = "v0-1-9"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.9/hello.zip"
  s3_bucket = "436614339345-wh-terraform-hello"
}

module "lambda_serverless_v0_1_10" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-10"
  describe_function = "Hello Serverless Terraform v0.1.9"

  subdomain = "v0-1-10"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.10/hello.zip"
  s3_bucket = "436614339345-wh-terraform-hello"
}